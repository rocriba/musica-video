package cat.dam.rocriba.soivideo;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.VideoView;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Inicialize Variables
        Button local = (Button) findViewById(R.id.btn_local);
        Button remote = (Button) findViewById(R.id.btn_remote);
        VideoView visor = (VideoView) findViewById(R.id.vv_video);


        //Start this sing when the app starts
        MediaPlayer so_inici = MediaPlayer.create(MainActivity.this, R.raw.pullmeunder);
        so_inici.start();

        //If local video button is presed, play video in local
        local.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                so_inici.stop();

                visor.setVisibility(View.VISIBLE);
                Uri video = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.videotest);
                visor.setVideoURI(video);
                visor.setMediaController(new MediaController(MainActivity.this));
                visor.start();
            }
        });

        //If remote video button is pressed, set the html address and play the video
        remote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                so_inici.stop();
                visor.setVisibility(View.VISIBLE);
              MediaController mediaController = new MediaController(MainActivity.this);
              Uri video = Uri.parse("https://developers.google.com/training/images/tacoma_narrows.mp4");
              visor.setMediaController(mediaController);
              visor.setVideoURI(video);
              visor.start();

            }
        });




    }
}
